function generateExpression(multiplicand, multiplier) {
   return "";
}

function generateLine(lineStart, lineEnd) {
    return "";
}

function generateAllLines(start, end) {
    return "";
}

function isValid(start, end) {
   return false;
}

export default {
    generateExpression,
    generateLine,
    generateAllLines,
    isValid
};
